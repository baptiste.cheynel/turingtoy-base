from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)

# La fonction exécute la machine de Turing.
def run_turing_machine(machine: Dict, input_: str, steps: Optional[int] = None) -> Tuple[str, List, bool]:

    # On initialise l'état, la bande, la tete de lecture et le compteur d'étapes.
    state, tape, head, step_count = machine['start state'], list(input_), 0, 0
    final_states = machine['final states']
    execution_history = []


    while True:

        # Si l'état est un état final on retire les espaces vides de la bande
        # et on retourne le résultat
        if state in final_states:
            tape = remove_blanks(tape, machine['blank'])
            return ''.join(tape), [machine, input_, ''.join(tape), execution_history], True

        # Affichage du rapport d'étape.
        print(f"Rapport {step_count}: {''.join(tape)} state = {state}")

        # On exécute une étape.
        state, tape, head, execution_history = step_turing_machine(machine, state, tape, head, execution_history)
        step_count += 1

        # Si le nombre d'étapes dépasse 400, on arrête l'exécution
        # et on retourne le résultat.
        if step_count > 400:
            print("fin du programme")
            tape = remove_blanks(tape, machine['blank'])
            return ''.join(tape), [machine, input_, ''.join(tape), execution_history], False

        # Si le nombre d'étapes est atteint, on arrête l'exécution
        # et on retourne le résultat.
        if steps is not None and step_count >= steps:
            tape = remove_blanks(tape, machine['blank'])
            return ''.join(tape), [machine, input_, ''.join(tape), execution_history], False



# La fonction exécute une étape de la machine de Turing
def step_turing_machine(machine, state, tape, head, execution_history):

    # On lit le caractère courant sur la bande.
    current_char = tape[head]
    print(f"current_char = {current_char}")

    # On récupère la règle à appliquer
    rule = machine['table'][state].get(current_char)
    print(f"rule = {rule}")
    if not rule:
        raise Exception(f"No rule defined for state '{state}' and character '{current_char}'")

    # On ajoute l'étape à l'historique
    execution_history.append({'state': state, 'reading': current_char, 'position': head, 'memory': ''.join(tape), 'transition': rule})

    # Si la règle contient une instruction d'ecriture.
    if 'write' in rule:
        tape[head] = rule.get('write', current_char)

    # Si la règle contient une instruction de deplacement vers la gauche.
    if 'L' in rule:
        head -= 1
        if head == -1:
            tape.insert(0, machine['blank'])
            head = 0
        if isinstance(rule, dict):
            state = rule.get('L', state)

    # Si la regle contient une instruction de déplacement vers la droite
    elif 'R' in rule:
        head += 1
        if head == len(tape):
            tape.append(machine['blank'])
        if isinstance(rule, dict):
            state = rule.get('R', state)

    return state, tape, head, execution_history

#Supprime les espaces vides
def remove_blanks(tape: List[str], blank: str) -> List[str]:
    return list(''.join(tape).strip(blank))
